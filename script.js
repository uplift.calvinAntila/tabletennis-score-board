// select the elements and declare a variable
const reset = document.querySelector('#reset');
let winScore = document.querySelector('#winning-score');
const set = document.querySelector('#set');
const scoreBoard = document.querySelector('#score-board');
let winningScore = parseInt(winScore.value);
let selectedScore = parseInt(winScore.value);
let gameScore = 3;

// create a object for player 1 and player 2 to group them together
p1 = {
   score: 0,
   gameSetScore: 0,
   display: document.querySelector('#p1Display'),
   button: document.querySelector('#player1'),
   gameSetDisplay: document.querySelector('#p1-game-score')
}
p2 = {
   score: 0,
   gameSetScore: 0,
   display: document.querySelector('#p2Display'),
   button: document.querySelector('#player2'),
   gameSetDisplay: document.querySelector('#p2-game-score')
}
// event to get the value for Game Set
set.addEventListener('change', function () {
   gameScore = parseInt(this.value);
   console.log(gameScore)
})
// event to get the value for Winning Score
winScore.addEventListener('change', function (){
   winningScore = parseInt(this.value);
   selectedScore = parseInt(this.value);
   console.log(winningScore)
})
// event button to add scores for player 1 and player 2
//it will call the function addScore
p1.button.addEventListener('click', function () {
   addScore(p1,p2);
   tieBreaker(p1,p2);
   displayWinner();
   selectDisabled();
   resetBtnDisabled();
})
p2.button.addEventListener('click', function () {
   addScore(p2,p1);
   tieBreaker(p2,p1);
   displayWinner();
   selectDisabled();
   resetBtnDisabled();
   })
// event for reset button it will just call the resetbutton function
reset.addEventListener('click', resetbutton);
// function to update the score of player 1 and player 2
function addScore(player, opponent) {
   player.score += 1;
   if (player.score === winningScore) {
      player.button.disabled = true;
      opponent.button.disabled = true;
      player.display.classList.add('winner');
      opponent.display.classList.add('loser');
      gameSetScore(player, opponent);
   }
   player.display.textContent = player.score;
}
// for a tie breaker
function tieBreaker(player,opponent) {
   if (player.score === opponent.score && player.score === winningScore - 1) {
      winningScore++;
      
      winScore.selectedOptions[0].value = winningScore;
      winScore.classList.add('overtime');
      winScore.selectedOptions[0].textContent = `TIE IN ${winningScore}`;
   
      console.log('tie breaker' + winningScore);

   }
}
//function to update the Game Set Score
function gameSetScore(player, opponent) {
   player.gameSetScore += 1;
   if (player.gameSetScore === gameScore) {
      player.gameSetDisplay.classList.add('winner');
      opponent.gameSetDisplay.classList.add('loser');
      setTimeout(resetGame, 3000);
      setTimeout(resetbutton, 3000);
      setTimeout(selectDisabled, 3000);
   }
   player.gameSetDisplay.textContent =  player.gameSetScore;
}
// function to reset the game score
function resetbutton() {
   for (let p of [p1,p2]) {
      p.score = 0;
      p.display.textContent = 0;
      p.button.disabled = false;
      p.display.classList.remove('winner', 'loser');
   }
      // winScore.selectedIndex = index;
      winningScore = selectedScore;
      winScore.classList.remove('overtime');
      winScore.selectedOptions[0].value = selectedScore;
      winScore.selectedOptions[0].textContent = selectedScore;
}
// function to reset the game for the Game Set Winner
 function resetGame() {
    for (let p of [p1,p2]) {
      p.gameScore = 0;
      p.gameSetScore = 0;
      p.gameSetDisplay.textContent = 0;
      p.gameSetDisplay.classList.remove('winner', 'loser');
    }
   }
// Create element to show the winner
const winner = document.createElement('div');
winner.classList.add('winnerDisplay');
const winnnerText = document.createElement('h2');
// Function to display the player winner and remove after 3 seconds
   function addWinner() {
        winner.append(winnnerText);
        scoreBoard.append(winner);

        function removeDisplay() {
            scoreBoard.removeChild(winner);
        }
        setTimeout(removeDisplay, 3000);
   }
// Function to to get the game set winner
   function displayWinner() {
       if (p1.gameSetScore === gameScore) {
        winnnerText.textContent = `Player 1 win! Congratulations...`;
        console.log('player 1 win');
        addWinner();
       }    
       else if (p2.gameSetScore === gameScore) {
        winnnerText.textContent = `Player 2 win! Congratulations...`;
        console.log('player 2 win');
        addWinner();
        }
    }
// disbaled the set score and game set score once the game start
    function selectDisabled() {
      if (p1.score !== 0 || p2.score !== 0) {
         winScore.disabled = true;
         set.disabled = true;
      }
      else {
         winScore.disabled = false;
         set.disabled = false;
      }
    }
// disbaled the reset once the game start
    function resetBtnDisabled() {
      if (p1.score === winningScore || p2.score === winningScore) {
         reset.disabled = false;
      }
      else {
         reset.disabled = true;
      }
   }
